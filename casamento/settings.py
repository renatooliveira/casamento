
# coding: utf-8
import os
import dj_database_url
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4sa^#)$z8pi!f#*wyrbm%)wpet01iu0m%r$np7k3)b$o)&r%$r'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'core',
    'south',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'casamento.urls'

WSGI_APPLICATION = 'casamento.wsgi.application'

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME')


if AWS_ACCESS_KEY_ID:
    DEFAULT_FILE_STORAGE = 'casamento.s3utils.MediaRootS3BotoStorage'
    STATICFILES_STORAGE = 'casamento.s3utils.StaticRootS3BotoStorage'
    MEDIA_URL = 'https://mobdoctor.s3.amazonaws.com/media/'
    STATIC_URL = 'https://mobdoctor.s3.amazonaws.com/static/'
else:
    STATIC_URL = '/leo_static/'

    MEDIA_URL = '/leo_media/'

DATABASES = {}

DATABASES['default'] = dj_database_url.config()

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Recife'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = 'leo_media/'
STATIC_ROOT = 'leo_static/'


try:
    from local_settings import *
except:
    pass

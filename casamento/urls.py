from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from core.models import Guest, Gift


class GuestInline(admin.StackedInline):
    model = Guest


class GiftAdmin(admin.ModelAdmin):
    inlines = (GuestInline, )


admin.autodiscover()

admin.site.register(Gift, GiftAdmin)

urlpatterns = patterns(
    '',
    url(r'^$', 'core.views.index', name='index'),
    url(r'^nossa-historia/$', 'core.views.history', name='nossa-historia'),

    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

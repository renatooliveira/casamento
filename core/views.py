#coding: utf-8

from django.shortcuts import render, redirect
from .forms import GuestForm
from django.core.urlresolvers import reverse
from django.contrib import messages


def index(request):
    form = GuestForm(request.POST or None)
    if form.is_valid():
        instance = form.save()
        instance.save()
        instance.gift.available_quantity -= 1
        instance.gift.save()
        messages.success(request, u'Obrigado e nos vemos no casório!')
        redirect(reverse('index'))
    return render(request, 'index.html', {'form': form})


def history(request):
    return render(request, 'history.html', {})

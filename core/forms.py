#coding: utf-8

from django import forms
from .models import Guest, Gift


from django.forms import models
from django.forms.fields import ChoiceField


class AdvancedModelChoiceIterator(models.ModelChoiceIterator):
    def choice(self, obj):
        return (self.field.prepare_value(obj),
                self.field.label_from_instance(obj), obj)


class AdvancedModelChoiceField(models.ModelChoiceField):
    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices

        return AdvancedModelChoiceIterator(self)

    choices = property(_get_choices, ChoiceField._set_choices)


class GuestForm(forms.ModelForm):

    gift = AdvancedModelChoiceField(
        label="Presente escolhido",
        queryset=Gift.objects.filter(available_quantity__gt=0),
        widget=forms.RadioSelect
    )

    class Meta:
        model = Guest




# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Gift'
        db.create_table(u'core_gift', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('available_quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'core', ['Gift'])

        # Adding model 'Guest'
        db.create_table(u'core_guest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('gift', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Gift'])),
        ))
        db.send_create_signal(u'core', ['Guest'])


    def backwards(self, orm):
        # Deleting model 'Gift'
        db.delete_table(u'core_gift')

        # Deleting model 'Guest'
        db.delete_table(u'core_guest')


    models = {
        u'core.gift': {
            'Meta': {'object_name': 'Gift'},
            'available_quantity': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'core.guest': {
            'Meta': {'object_name': 'Guest'},
            'gift': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Gift']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        }
    }

    complete_apps = ['core']
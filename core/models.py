#coding: utf-8

from django.db import models


class Gift(models.Model):
    name = models.CharField(u'Nome', max_length=500)
    available_quantity = models.IntegerField(u'Quantidade disponível')
    picture = models.ImageField(
        u'Imagem', upload_to='img', null=True, blank=True
    )
    price = models.DecimalField(u'Preço', max_digits=6, decimal_places=2)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        verbose_name = u'Presente'


class Guest(models.Model):
    name = models.CharField(u'Nome', max_length=500)
    gift = models.ForeignKey(Gift, verbose_name=u'Presente escolhido')

    def __unicode__(self):
        return u'{0} - {1}'.format(self.name, self.gift.name)

    class Meta:
        verbose_name = u'Convidado'
